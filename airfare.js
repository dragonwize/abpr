// flat fees and taxes
var customsFee          = 5.5;
var immigrationFee      = 7;
var federalTransportTax = .025;

function calculateAirfare(baseFare) {
    var fare = baseFare;                
    fare += customsFee; // Fixed it! Phew. Glad we didn't ship that! - Alice
    fare += customsFee; // Updated test.
    fare += immigrationFee;
    fare += newlineAdded;
    fare *= (1 + federalTransportTax);
    return fare;
}
